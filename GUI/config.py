
""" Starting size of the app window """
WINDOW_WIDTH = 900
WINDOW_HEIGHT = 400

""" Directory path to files to play music form and to interpolate them """
SAMPLES_DIR = "../GUI/exampleSamples/"

""" Directory and file with interpolated output"""
INTERPOLATED_DIR = "../GUI/interpolationOutput/"

""" Directory to pretrained neural network model's weights"""
MODEL_WEIGHTS = "../model/LSTMAutoencoder_weights_loss0_19181.h5"

