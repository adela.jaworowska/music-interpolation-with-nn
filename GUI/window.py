import datetime
import glob
import os
from tkinter import *
from tkinter import ttk, filedialog
import mido
from mido import MidiFile
from music21 import converter
from GUI.config import *
from midiScripts.interpolator import Interpolator
from model.LSTMAutoencoder_loss0_19181 import Autoencoder

class Window(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        self.init_window()

        self.output = mido.open_output()

        self.statusLabel.config(text="Welcome!", fg="#000066", font="bold", width=400)

        self.interpolator = Interpolator()
        self.autoencoder = Autoencoder(len(self.interpolator.fprc))

        (self.autoencoder.model).load_weights(MODEL_WEIGHTS)


    #Creation of init_window
    def init_window(self):

        # set the title of our master widget
        self.master.title("MusicBAE-BestAppEver")

        # add main pane which is background for the rest elements. It contains many other panes with elements
        self.main_pane()

        self.SAMPLE_1_PATH = ""
        self.SAMPLE_2_PATH = ""
        self.INTERPOLATION_RESULT = ""


    def main_pane(self):
        main_pane = PanedWindow(orient=HORIZONTAL)
        main_pane.configure(bg="#000066")
        main_pane.pack(fill=BOTH, expand=1)

        self.settings_menu_pane(main_pane)
        self.interpolation_main_pane(main_pane)

    def settings_menu_pane(self, master = None):
        settings_menu_pane = PanedWindow(master, orient=VERTICAL)
        settings_menu_pane.pack(side=LEFT, fill=Y)
        side_menu_title = self.header("SETTINGS", settings_menu_pane)

        side_menu_subtitle1 = self.small_header('Interpolation length:', settings_menu_pane)
        self.sliderInterpolationLength = Scale(settings_menu_pane, from_=2, to=16, orient=HORIZONTAL)

        space = Message(settings_menu_pane)

        settings_menu_pane.add(side_menu_title)
        settings_menu_pane.add(space)
        settings_menu_pane.add(side_menu_subtitle1)
        settings_menu_pane.add(self.sliderInterpolationLength)

    def interpolation_main_pane(self, master = None):
        interpolation_main_pane = PanedWindow(master, orient=HORIZONTAL)
        interpolation_main_pane.pack(fill=BOTH, expand = 1)

        self.interpolation_result_pane(interpolation_main_pane)
        self.interpolation_play_pane(interpolation_main_pane)
        self.samples_pane(interpolation_main_pane)

    def interpolation_result_pane(self, master = None):
        interpolation_result_pane = PanedWindow(master, orient=VERTICAL)
        interpolation_result_pane.config(bg="#000066")
        interpolation_result_pane.pack(side=TOP, fill=BOTH, expand=1)

        header = self.header("INTERPOLATION RESULT", interpolation_result_pane)
        header.pack(side=TOP)

        self.statusLabel = Message(interpolation_result_pane, text="")
        self.statusLabel.pack(fill=BOTH, expand=1)

    def interpolation_play_pane(self, master = None):
        interpolation_play_pane = PanedWindow(master, orient=HORIZONTAL)
        interpolation_play_pane.config(bg="#D5E8F1")
        interpolation_play_pane.pack(fill=X)

        interpolation_button = self.interpolation_button(interpolation_play_pane)
        interpolation_button.pack(side=LEFT, anchor=S, padx=[150, 0])

        self.warning_label_0 = self.label(interpolation_play_pane, "")
        self.warning_label_0.config(bg="#D5E8F1")
        self.warning_label_0.pack(side=RIGHT, anchor=S, padx=[0, 10])

        show_interpolation_result_pianoroll_button = self.show_interpolation_result_pianoroll_button(interpolation_play_pane)
        show_interpolation_result_pianoroll_button.pack(side=RIGHT, anchor=S, padx=[0, 10])

        play_result_button = self.play_result_button(interpolation_play_pane)
        play_result_button.pack(side = RIGHT, anchor = S, padx = [0, 20])



    def samples_pane(self, master = None):
        samples_pane = PanedWindow(master, orient=HORIZONTAL)
        samples_pane.config(bg="#666666")
        samples_pane.pack(fill=X)

        samples_pane.add(self.first_sample_pane(samples_pane))
        samples_pane.add(self.second_sample_pane(samples_pane))


    def first_sample_pane(self, master = None):
        first_sample_pane = PanedWindow(master)

        header = self.header("Sample 1", first_sample_pane)
        header.pack(fill=X)

        choose_file_button = self.browse_directory_button_1(first_sample_pane)
        choose_file_button.pack(fill=X)

        self.label_1 = self.label(first_sample_pane, "no file selected")
        self.label_1.pack(fill=X, expand=1)

        self.warning_label_1 = self.label(first_sample_pane)
        self.warning_label_1.pack(fill=X)

        self.play_button_1 = self.play_button_1(first_sample_pane)
        self.play_button_1.pack(fill=X)

        self.show_sample1_pianoroll = self.show_sample1_pianoroll_button(first_sample_pane)
        self.show_sample1_pianoroll.pack(fill=X)

        return first_sample_pane

    def second_sample_pane(self, master = None):
        second_sample_pane = PanedWindow(master)

        header = self.header("Sample 2", second_sample_pane)
        header.pack(fill=X)

        choose_file_button = self.browse_directory_button_2(second_sample_pane)
        choose_file_button.pack(fill=X)

        self.label_2 = self.label(second_sample_pane, "no file selected")
        self.label_2.pack(fill=X, expand=1)

        self.warning_label_2 = self.label(second_sample_pane)
        self.warning_label_2.pack(fill=X)

        self.play_button_2 = self.play_button_2(second_sample_pane)
        self.play_button_2.pack(fill=X)

        self.show_sample2_pianoroll = self.show_sample2_pianoroll_button(second_sample_pane)
        self.show_sample2_pianoroll.pack(fill=X)

        return second_sample_pane

    def header(self, title, master = None):
        Msg = title
        header_title = Message(master, text=Msg, fg="white", font="bold")
        header_title.config(bg="#000066", width=300)
        # header_title.pack()

        return header_title

    def small_header(self, title, master = None):
        Msg = title
        subtitle = Message(master, text=Msg)
        subtitle.config(bg="#E0E0E0", width=300)
        subtitle.pack()

        return subtitle

    def interpolation_button(self, master = None):
        return Button(master, text="Interpolate", command=self.interpolate)

    def play_button_1(self, master=None):
        return Button(master, text="Play", command = lambda: self.play(self.SAMPLE_1_PATH, 1))

    def play_button_2(self, master=None):
        return Button(master, text="Play", command = lambda: self.play(self.SAMPLE_2_PATH, 2))

    def play_result_button(self, master=None):
        return Button(master, text="Play", command = lambda: self.play(self.INTERPOLATION_RESULT, 0))

    def show_sample1_pianoroll_button(self, master=None):
        return Button(master, text="Show piano roll", command=lambda: self.show_pianoroll(self.SAMPLE_1_PATH, 1))

    def show_sample2_pianoroll_button(self, master=None):
        return Button(master, text="Show piano roll", command=lambda: self.show_pianoroll(self.SAMPLE_2_PATH, 2))

    def show_interpolation_result_pianoroll_button(self, master=None):
        return Button(master, text="Show piano roll", command=lambda: self.show_pianoroll(self.INTERPOLATION_RESULT, 0))

    def browse_directory_button_1(self, master = None):
        return Button(master, text="Select file", command = lambda: self.browse_directory(1))

    def browse_directory_button_2(self, master=None):
        return Button(master, text="Select file", command = lambda: self.browse_directory(2))

    def label(self, master = None, text=""):
        return Label(master, text=text)

    def warning_label_on(self, label_number):
        if label_number == 0:
            self.warning_label_0.config(text="Interpolation not performed yet!", fg="red")
        if label_number == 1:
            self.warning_label_1.config(text="File not selected!", fg="red")
        if label_number == 2:
            self.warning_label_2.config(text="File not selected!", fg="red")

    def warning_label_off(self, label_number):
        if label_number == 0:
            self.warning_label_0.config(text="")
        if label_number == 1:
            self.warning_label_1.config(text="")
        if label_number == 2:
            self.warning_label_2.config(text="")


    def browse_directory(self, sample_number):
        self.statusLabel.config(text="")
        result = filedialog.askopenfilename(initialdir=SAMPLES_DIR, title="Select sample", filetype=((".mid", "*.mid"), ("All files", "*.*")))
        if sample_number == 1:
            self.label_1.config(text=str(self.extract_file_name_from_path(result)))
            self.SAMPLE_1_PATH = result
            self.warning_label_off(1)
        else:
            self.label_2.config(text=str(self.extract_file_name_from_path(result)))
            self.SAMPLE_2_PATH = result
            self.warning_label_off(2)

        self.INTERPOLATION_RESULT = ""

    def play(self, file_path, sample_number):
        try:
            for msg in MidiFile(file_path).play():
                self.output.send(msg)
        except:
            self.warning_label_on(sample_number)

    def extract_file_name_from_path(self, path):
        return str(path).split('/')[-1:][0]

    def clear_interpolation_rsult_files(self):
        for file in glob.glob(INTERPOLATED_DIR + "*.mid"):
            os.remove(file)

    def create_file_name_for_interpolation_output(self, sample_1_path, sample_2_path, interpolation_len):
        date = datetime.datetime.today()
        return str(self.extract_file_name_from_path(sample_1_path)[:-4]) + "_" + \
               str(self.extract_file_name_from_path(sample_2_path)[:-4]) + "_" + \
               str(interpolation_len) + "_" + str(date.year) + "_" + str(date.month) + "_" + str(date.day) + "_" + \
               str(date.hour) + "_" + str(date.minute) + "_" + str(date.second) + ".mid"

    def show_pianoroll(self, path, sample_number):
        try:
            s = converter.parse(path)
            s.plot('pianoroll')
        except:
            self.warning_label_on(sample_number)

    def interpolate(self):
        try:
            self.statusLabel.config(text="LOADING...", fg="#000066", font="bold", width=150)
            interpolation_length = self.sliderInterpolationLength.get()

            output_file_name = self.create_file_name_for_interpolation_output(self.SAMPLE_1_PATH, self.SAMPLE_2_PATH, interpolation_length)
            print("before interpolation")
            self.interpolator.interpolate(self.SAMPLE_1_PATH, self.SAMPLE_2_PATH, self.autoencoder.encoder,
                                          self.autoencoder.decoder, interpolation_length, INTERPOLATED_DIR, output_file_name)
            self.INTERPOLATION_RESULT = INTERPOLATED_DIR + output_file_name
            print("after interpolation")
            self.warning_label_0.config(text="")
            self.statusLabel.config(text="INTERPOLATION DONE!", fg="#000066", font="bold", width=300)

        except:
            self.statusLabel.config(text="")
            if self.SAMPLE_1_PATH == "":
                self.warning_label_on(1)
            if self.SAMPLE_2_PATH == "":
                self.warning_label_on(2)



