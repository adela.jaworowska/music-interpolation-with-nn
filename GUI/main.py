from tkinter import *
from GUI.window import Window
from GUI.config import *

root = Tk()

#size of the window
root.geometry(str(WINDOW_WIDTH) + "x" + str(WINDOW_HEIGHT))
root.resizable(height = None, width = None)

app = Window(root)

root.mainloop()
