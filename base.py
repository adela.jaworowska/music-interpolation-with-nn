
def get_training_data_dir():
    # return "data/musicDataFakedForBiggerSetL16Chords/1000samples/train/"
    return "data/96TimeQuantum_traditionalmusic/train/"


def get_validation_data_dir():
    # return "data/musicDataFakedForBiggerSetL16Chords/1000samples/validate/"
    return "data/96TimeQuantum_traditionalmusic/validate/"


def get_test_data_dir():
    # return "data/musicDataFakedForBiggerSetL16Chords/1000samples/test/"
    return "data/96TimeQuantum_traditionalmusic/test/"


def get_neural_network_output_dir():
    return "data/musicDataFakedForBiggerSetL16Chords/1000samples/nnOutput/"


def get_file_with_notes_dir():
    return "/notes.txt"


def get_sample_1_to_interpolate_dir():
    return "data/interpolation/3/input_sample_1/"


def get_sample_2_to_interpolate_dir():
    return "data/interpolation/3/input_sample_2/"


def get_interpolation_output_sequence_dir():
    return "data/interpolation/3/output_sequence/"


def get_interpolation_concatenated_output_sequence_dir():
    return "data/interpolation/3/output_sequence_concatenated/"

