from numpy import array
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import Callback
import tensorflow.keras as keras
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model, Sequential, load_model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import Input, LSTM, Dense, RepeatVector, TimeDistributed, Activation, Reshape, Masking
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
from IPython.display import clear_output
from midiScripts.full_piano_roll_converter import FullPianoRollConverter
import base


class Autoencoder:

    def __init__(self, chords_len):
        self.chords_len = chords_len  # length of one-hot encoded vector representing one note
        self.seq_window = 16  # length of notes in one music sample
        self.lstm_state = 4048  # length of narrow lstm cells output

        self.encoder, self.decoder, self.model = self.autoencoder_model()

    def autoencoder_model(self):
        # # define encoder
        enc_inputs1 = Input(shape=(None, 3, self.chords_len))
        enc_inputs2 = Reshape((-1, self.chords_len * 3), input_shape=(None, 3, self.chords_len))(enc_inputs1)
        enc_output = LSTM(self.lstm_state, return_sequences=False, return_state=False)(enc_inputs2)

        encoder = Model(enc_inputs1, enc_output, name="encoder")

        # define decoder
        dec_inputs = Input(shape=encoder.output.shape[1:])
        dec_inputs_exp = tf.expand_dims(dec_inputs, axis=1)
        dec_repeated = tf.tile(dec_inputs_exp, [1, tf.shape(enc_inputs1)[1], 1])
        dec_lstm = LSTM(self.lstm_state, return_sequences=True)(dec_repeated)
        play_output = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="play_output")(dec_lstm)
        repeat_output = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="repeat_output")(dec_lstm)
        volume_output = TimeDistributed(Dense(self.chords_len, activation='sigmoid'), name="volume_output")(dec_lstm)

        concat_output = tf.stack([play_output, repeat_output, volume_output], axis=-2)

        decoder = Model(inputs=[dec_inputs, enc_inputs1], outputs=concat_output, name="decoder")

        print(concat_output)

        model_encoded = encoder(enc_inputs1)
        model_decoded = decoder([model_encoded, enc_inputs1])
        model = Model(enc_inputs1, outputs=model_decoded, name="autoencoder")

        return encoder, decoder, model

    def load_my_model(self, model_weights):
        # load previously saved weights to the model
        self.model.load_weights(model_weights)
