W każdym z folderów oznaczonych numerami 1,2,3,4 znajduję się rezultat kilku interpolacji(o długościach: 3,5,7,9) dwóch kawałków muzycznych.

Każdy folder zawiera pliki:

input1.mid, input2.mid - fragmenty wejściowe w formacie MIDI
input1_pianoroll.png, input2_pianoroll.png - fragmenty wejściowe w formacie PNG (piano roll)
out_interpolation_length_[nr].mid - zinterpolowane fragmenty, przy długości interpolacji [nr] w formacie MIDI
out_interpolation_length_[nr]_pianoroll.png - zinterpolowane fragmenty, przy długości interpolacji [nr] w formacie PNG (piano roll)
