import numpy as np
from midiScripts.full_piano_roll_converter import FullPianoRollConverter


class Interpolator:
    """
    Class  used to perform interpolation.
    """

    def __init__(self):
        self.fprc = FullPianoRollConverter()

    @staticmethod
    def linear_interpolation(encoded_sample_1, encoded_sample_2, interpolation_length):
        interpolated_output = []
        for i in range(0, interpolation_length):
            print(i)
            mi = i/(interpolation_length-1)
            zi = (1-mi)*encoded_sample_1 + mi*encoded_sample_2
            interpolated_output.append(zi)

        return interpolated_output

    def interpolate(self, sample_1_path, sample_2_path, encoder, decoder, interpol_length, interpolation_concatenated_output_dir, output_file_name):
        samp1 = self.fprc.convert_midi_to_neural_network_input(sample_1_path)
        samp2 = self.fprc.convert_midi_to_neural_network_input(sample_2_path)

        samp1_encoded = encoder.predict(samp1)
        samp2_encoded = encoder.predict(samp2)

        interpolation_length = interpol_length

        sequence = self.linear_interpolation(samp1_encoded, samp2_encoded, interpolation_length)

        decoded = []
        for elem in sequence:
            temp = decoder.predict([elem, samp1])
            decoded.append(temp)

        decoded = np.array(decoded)

        interpolated_result = decoded.reshape((1, decoded[0][0].shape[0] * decoded.shape[0], 3, 89))

        # convert interpolated result to midi and save it to file
        self.fprc.convert_one_neural_network_output_sample_to_midi(
        interpolated_result[0], interpolation_concatenated_output_dir + output_file_name)


if __name__ == '__main__':
    pass