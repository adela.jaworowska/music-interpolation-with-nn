import glob
import math
import random
import os
from music21 import stream, converter, instrument, note, chord, duration
from tqdm import tqdm
import numpy as np
import tensorflow as tf
from midiScripts.full_piano_roll_converter import FullPianoRollConverter



class MidiPreprocessor:
    """
    Class consist functions to preprocess midi files.
    It was used to examine data better and prepare data for the project
    """

    def __init__(self):
        self.fprc = FullPianoRollConverter()

    @staticmethod
    def save_all_occuring_notes_and_chords_to_file(directory):
        print("Collecting all notes and chords from data set started...")
        notes_set = set()

        for file in tqdm(glob.glob(directory + "*.mid")):
            midi = converter.parse(file)

            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                # print("heloooo: " + len(s2.parts))
                notes_to_parse = s2.parts[0].recurse()
                print("try")
            except:  # file has notes in a flat structure
                print("except")
                notes_to_parse = midi.flat.notes

            for e in notes_to_parse:
                print(str(e) + " " + str(e.offset) + " " + str(e.duration))

            for element in notes_to_parse:
                if isinstance(element, note.Note):
                    notes_set.add(str(element.pitch))
                elif isinstance(element, chord.Chord):
                    notes_set.add('.'.join(str(n.pitch) for n in element))
                elif isinstance(element, note.Rest):
                    pass
                else:
                    pass

        # with open("notes3.txt", "a") as txtfile:
        #     txtfile.writelines(map(lambda x: str(x) + "\n", notes_set))
        # print("All notes and chords collected and saved to file.")

    @staticmethod
    def turn_chord_into_note(_chord):
        """
        :param _chord: chord
        :return: one note extracted from chord
        """
        return note.Note(_chord[0].pitch)

    def create_music_data_without_chords(self):
        i = 0
        for file in glob.glob("musicData/*.mid"):
            i += 1
            s = stream.Stream()
            midi = converter.parse(file)

            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            for element in notes_to_parse:
                if isinstance(element, note.Note):
                    s.append(element)
                elif isinstance(element, chord.Chord):
                    pass
                    new_note = self.turn_chord_into_note(element)
                    s.append(new_note)
                else:
                    pass
            # s.show('midi')
            s.write('midi', "musicDataChordsRemoved/file" + str(i) + ".mid")

    @staticmethod
    def get_min_music_file_length_in_notes_or_chords():
        min_length = 1000000
        for file in glob.glob("musicDataChordsRemoved/*.mid"):
            midi = converter.parse(file)
            note_counter = 0
            try:
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:
                notes_to_parse = midi.flat.notes

            for element in notes_to_parse:
                if isinstance(element, note.Note):
                    note_counter += 1
                else:
                    pass
            if note_counter <= min_length:
                min_length = note_counter

        return min_length

    @staticmethod
    def get_number_of_songs_of_length_at_last_x_notes_or_chords(x, directory):
        song_counter = 0
        for file in glob.glob(directory + "*.mid"):
            midi = converter.parse(file)
            try:
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:
                notes_to_parse = midi.flat.notes

            if len(notes_to_parse) >= x:
                song_counter += 1

        return song_counter


    @staticmethod
    def generate_fake_data_samples_of_length_x_note_or_chords(x):
        """ Cuts all x notes-long music segments from music samples and saves every of them to separate file"""
        for file in glob.glob("moreData/*.mid"):
            midi = converter.parse(file)
            print("parsing: " + str(file))
            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            num_of_samples = len(notes_to_parse) - x
            for i in range(0, num_of_samples):
                s = stream.Stream()
                for j in range(i, i + x):
                    if isinstance(notes_to_parse[j], note.Note):
                        s.append(notes_to_parse[j])
                    if isinstance(notes_to_parse[j], chord.Chord):
                        s.append(notes_to_parse[j])
                    else:
                        pass
                s.write('midi', "outt/" + str(i) + str(j) + str(random.randint(1, 20)) + ".mid")

    def get_sample_length_in_time_quantums(self, notes_chords_list, time_quantum):
        max_offset = notes_chords_list[-1].offset / time_quantum
        last_length = notes_chords_list[-1].duration.quarterLength / time_quantum

        offset_decimal, offset_complete = math.modf(max_offset)
        length_decimal, length_complete = math.modf(last_length)

        hole_sample_length = (offset_complete + length_complete)

        if offset_decimal != 0:
            if (offset_decimal * time_quantum) - (time_quantum / 2) > 0:
                hole_sample_length += 1

        if length_decimal != 0:
            if (offset_decimal * time_quantum) - (time_quantum / 2) > 0:
                hole_sample_length += 1

        return int(hole_sample_length)

    def get_elements_offset_in_time_quantums(self, element, time_quantum):
        """ returns time quantum where element starts """
        elem_offset_decimal, elem_offset_complete = math.modf(element.offset / time_quantum)
        element_start = elem_offset_complete
        if (elem_offset_decimal * time_quantum) - (time_quantum / 2) > 0:
            element_start += 1
        return element_start

    def get_elements_volume(self, element):
        """ returns element's volume """
        element_volume = str(round(element.volume.velocityScalar, 2))

    def get_elements_duration(self, element, time_quantum):
        """ returns duration of given element """
        duration_decimal, duration_complete = math.modf(element.duration.quarterLength / time_quantum)
        element_duration = duration_complete

        if duration_decimal != 0:
            if (duration_decimal * time_quantum) - (time_quantum / 2) > 0:
                element_duration += 1
        return element_duration

    def generate_fake_data_samples_of_x_time_quantums_length(self, x):
        """ Cuts all x time quantum-long music segments from music samples and saves every of them to separate file"""
        for file in tqdm(glob.glob("all_maestro/*.mid")):
            midi = converter.parse(file)
            print(file)
            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            file_matrix = self.fprc.convert_notes_and_chords_to_three_dimension_matrix(notes_to_parse)

            i = 0
            while i < len(file_matrix)-x-1:

                # extract matrix with sample to save to file
                temp_matrix = file_matrix[i:i+x]

                # convert partial matrix to midi and save it to file
                s = self.fprc.convert_one_neural_network_output_sample_to_midi(temp_matrix, "", "")

                # save sample to file
                s.write('midi', "out_maestro/" + str(file).split('\\')[-1:][0] + str(i) + "_" + str(random.randint(1, 20)) + ".mid")


                # i += 96 # if time quantum is 0.25
                i += 288 # if time quantum is 1/12

    def cut_one_shorter_sample_of_length_x_form_every_file(self, x):
        i = 0
        for file in tqdm(glob.glob("all_moreData/*.mid")):

            try:
                midi = converter.parse(file)
            except:
                continue

            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            try:
                file_matrix = self.fprc.convert_notes_and_chords_to_three_dimension_matrix(notes_to_parse)
            except:
                continue

            if len(file_matrix) <= 96:
                continue

            temp_matrix = file_matrix[0:97]

            # convert partial matrix to midi and save it to file
            s = self.fprc.convert_one_neural_network_output_sample_to_midi(temp_matrix, "", "")

            # save sample to file
            s.write('midi', "out_moreData/" + str(file).split('\\')[-1:][0] + "_" + str(random.randint(1, 20)) + ".mid")

    @staticmethod
    def delete_files_different_than_x_notes(x, directory_path):

        for file in tqdm(glob.glob("temp/*.mid")):

            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            if len(notes_to_parse) != x:
                os.remove(file)
                print("removed")

    def delete_files_different_than_x_time_quantums(self, x):

        removed = 0
        cuted = 0
        for file in tqdm(glob.glob("out_moreData/*.mid")):
            try:
                midi = converter.parse(file)
            except:
                os.remove(file)
                continue

            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            file_matrix = self.fprc.convert_notes_and_chords_to_three_dimension_matrix(notes_to_parse)

            if len(file_matrix) < x:
                os.remove(file)
                removed += 1
                print("removed")

            elif len(file_matrix) > x:
                temp_matrix = file_matrix[0:96]

                # convert partial matrix to midi
                s = self.fprc.convert_one_neural_network_output_sample_to_midi(temp_matrix, "", "")

                # save sample to file
                s.write('midi', file)
                cuted += 1
        print("removed:" + str(removed))
        print("cuted:" + str(cuted))

    def print_samples_lengths(self):
        e = 0
        for file in glob.glob("out_moreData/*.mid"):
            try:
                midi = converter.parse(file)
            except:
                # os.remove(file)
                continue

            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            file_matrix = self.fprc.convert_notes_and_chords_to_three_dimension_matrix(notes_to_parse)
            print(len(file_matrix))

    @staticmethod
    def cut_song_length_to_x_notes(x):
        i = 0
        for file in tqdm(glob.glob("temp/*.mid")):
            i += 1
            s = stream.Stream()
            midi = converter.parse(file)
            print("parsing " + str(file))
            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            if len(notes_to_parse) < x:
                continue

            for element, j in zip(notes_to_parse, range(0, x)):
                if isinstance(element, note.Note):
                    s.append(element)
                elif isinstance(element, chord.Chord):
                    s.append(element)
                else:
                    pass
            # s.show('midi')
            s.write('midi', file)

    def concatenate_files(self, inputDirectory, outputDirectory):
        s = stream.Stream()
        for file in tqdm(glob.glob(inputDirectory + "*.mid")):
            midi = converter.parse(file)
            print(file)
            try:  # file has instrument parts
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
            except:  # file has notes in a flat structure
                notes_to_parse = midi.flat.notes

            for element in notes_to_parse:
                if isinstance(element, note.Note):
                    s.append(element)
                elif isinstance(element, chord.Chord):
                    s.append(element)

        s.write('midi', outputDirectory + "interpolatedOutput.mid")

    @staticmethod
    def rename_files():
        for file, i in zip(tqdm(glob.glob("moreData/*.mid")), range(0, 1000000)):
            # print(file)
            os.rename(file, str(i) + ".mid")

if __name__ == '__main__':
    mp = MidiPreprocessor()