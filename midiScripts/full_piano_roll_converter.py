import glob
import operator
import random
from music21 import note, converter, instrument, chord, stream, serial, corpus, midi, meter, tempo, duration, clef
from tqdm import tqdm
import numpy as np
import tensorflow as tf
import math
import pprint


class FullPianoRollConverter:
    """
    Class with functions to to preprocess music data for neural network.
    They can convert midi samples to three dimensional matrix and inversely.
    With use of music21 library midi file is converted to the list of music21 objects: notes, chords and rests.
    Proper attributes such as: pitches, duration, offset, volume are taken and processed.
    """

    """
    Lists to hold names of files converted to input for neural network
    to name it properly while converting back from nn output to midi after prediction
    """
    test_file_names = []
    train_file_names = []
    validate_file_names = []

    """
    These values are used to convert nn output to midi files 
    They are thresholds above which values form nn output value are rounded to 1
    """
    play_threshold = 0.5
    repeat_threshold = 0.5

    """
    Fraction which tells how you want to quantize music samples.
    1 is a duration of quarter note, so 0.25 is sixteenth note
    In data samples sometimes 1/3 values occures so time_quantum = 1/12 would be good
    but it will take much memory, so 0.25 is optimal
     """
    # time_quantum = 1/12
    time_quantum = 0.25

    def __init__(self):
        self.dictionary = self.create_notes_to_ints_dictionary()
        self.inverse_dictionary = self.create_ints_to_notes_dictionary()

    def __len__(self):
        return int(self.dictionary[max(self.dictionary.items(), key=operator.itemgetter(1))[0]]) + 1

    def create_notes_to_ints_dictionary(self):
        """
        :return: dictionary to convert note name to integer value
        """
        notes = ["C-", "C", "C#", "D-", "D", "D#", "E-", "E", "E#", "F-", "F", "F#", "G-", "G", "G#", "A-", "A", "A#", "B-", "B", "B#"]
        dictionary = {}
        for i in range(0, 9):
            for n in notes:
                my_pitch = n + str(i)
                # subtract 20 to make value of the first key in a piano keyboard ('A0') equal 1
                # value 0 is for rest
                dictionary.update({my_pitch : note.Note(my_pitch).pitch.ps - 20})
                len(dictionary)
        # delimit to 88 keys + rest
        self.cut_number_of_keyboard_keys(dictionary)
        return dictionary

    @staticmethod
    def cut_number_of_keyboard_keys(dictionary):
        """
        Deletes from dictionary notes which doesn't occur in a standard piano keyboard
        :param dictionary: that maps notes to integers
        """
        pitches_to_remove = ["C-0", "C0", "C#0", "D-0", "D0", "D#0", "E-0", "E0", "E#0", "F-0", "F0", "F#0",
                             "G-0", "G0", "G#0", "A-0", "C#8", "D-8", "D8", "D#8", "E-8", "E8", "E#8", "F-8", "F8",
                             "F#8", "G-8", "G8", "G#8", "A-8", "A8", "A#8", "B-8", "B8", "B#8"]
        for p in pitches_to_remove:
            dictionary.pop(p)

    def create_ints_to_notes_dictionary(self):
        """
        While creating this dictionary all notes with '#' are deleted and notes with '-' stands for them
        eg. G#5 -> A-5, D#2 -> E-2
        Keep this in mind while summarizing results from nn output

        :return: dictionary that maps integers to notes
        """

        inverse_dictionary = [(value, key) for key, value in self.dictionary.items()]
        return dict(inverse_dictionary)

    def convert_midi_to_neural_network_input(self, data_directory, names_to_save=""):
        """
        Converts midi files to neural network input in piano-roll format with volume and sound duration

        :param data_directory: path to directory with data samples
        :param names_to_save: optional, if you want to save names of files to list and use them later
        :return: all samples from directory converted to three dimensional matrix for nn
        """

        input = []
        for file in tqdm(glob.glob(data_directory)):
            self.save_file_name_to_list(names_to_save, file)
            partial_input = self.convert_one_midi_file_to_piano_roll_vector(file)
            input.append(partial_input)

        return np.array(input)

    @staticmethod
    def filter_music_sample(notes_to_parse):
        """
        :param notes_to_parse: music21 object consisting music21 elements extracted from midi files
        :return: list of music21 elements, only: notes, chords and rests
        """

        filtered_notes_to_parse = []
        for element in notes_to_parse:
            if isinstance(element, note.Note) or isinstance(element, chord.Chord) or isinstance(element, note.Rest):
                filtered_notes_to_parse.append(element)
        return filtered_notes_to_parse

    def convert_one_midi_file_to_piano_roll_vector(self, file):
        """
        Converts one midi file to neural network input in piano-roll format with volume and sound duration
        :param file: file in midi format
        :return: one sample converted to three dimensional matrix
        """
        midi = converter.parse(file)

        try:  # file has instrument parts
            s2 = instrument.partitionByInstrument(midi)
            notes_to_parse = s2.parts[0].recurse()
        except:  # file has notes in a flat structure
            notes_to_parse = midi.flat.notes

        if not notes_to_parse.isSorted:
            notes_to_parse.sorted

        file_matrix = self.convert_notes_and_chords_to_three_dimension_matrix(notes_to_parse)
        return np.array(file_matrix)

    def get_sample_length(self, notes_chords_list):
        """

        :param notes_chords_list: list of music21 objects that are in one music sample
        :return: length of the samaple in time quantums
        """
        max_offset = notes_chords_list[-1].offset / self.time_quantum
        last_length = notes_chords_list[-1].duration.quarterLength / self.time_quantum

        offset_decimal, offset_complete = math.modf(max_offset)
        length_decimal, length_complete = math.modf(last_length)

        hole_sample_length = (offset_complete + length_complete)

        if offset_decimal != 0:
            if (offset_decimal * self.time_quantum) - (self.time_quantum / 2) > 0:
                hole_sample_length += 1

        if length_decimal != 0:
            if (offset_decimal * self.time_quantum) - (self.time_quantum / 2) > 0:
                hole_sample_length += 1

        return int(hole_sample_length)

    def convert_notes_and_chords_to_three_dimension_matrix(self, notes_chords_list):
        """
        :param notes_chords_list: List of musci21 objects: notes, chords and rests for one music sample
        :return: Music21 objects converted to three dimensional matrix
        """

        sample_length = self.get_sample_length(notes_chords_list)

        hole_sample_matrix = np.zeros((sample_length, 3, 89))

        for element in notes_chords_list:

            if not isinstance(element, note.Note) and not isinstance(element, chord.Chord) and not isinstance(element, note.Rest):
                continue

            # get time quantum where element starts
            elem_offset_decimal, elem_offset_complete = math.modf(element.offset / self.time_quantum)
            element_start = elem_offset_complete
            if (elem_offset_decimal * self.time_quantum) - (self.time_quantum / 2) > 0:
                element_start += 1

            # get element's volume
            if not isinstance(element, note.Rest):
                element_volume = str(round(element.volume.velocityScalar, 2))
            # print("element volume: " + str(element_volume))

            # get pushed keys on the keyboard
            keys = []
            if isinstance(element, note.Note):
                keys.append(int(self.dictionary[str(element.nameWithOctave)]))
            elif isinstance(element, chord.Chord):
                for n in element:
                    keys.append(int(self.dictionary[str(n.pitch)]))
            if isinstance(element, note.Rest):
                keys.append(0)

            # get element's duration
            duration_decimal, duration_complete = math.modf(element.duration.quarterLength / self.time_quantum)
            element_duration = duration_complete

            if duration_decimal != 0:
                if (duration_decimal * self.time_quantum) - (self.time_quantum / 2) > 0:
                    element_duration += 1

            # special cases in more complex music samples where noes with duration equal 0 occure
            if element_duration == 0:
                element_duration = 1

            for quantum in range(0, int(element_duration)):

                # not sure if this if is correct
                if element_start + quantum >= len(hole_sample_matrix):
                    break

                for k in keys:
                    hole_sample_matrix[int(element_start + quantum)][0][k] = 1

                    if quantum == 0:
                        hole_sample_matrix[int(element_start + quantum)][1][k] = 1

                    if not isinstance(element, note.Rest):
                        hole_sample_matrix[int(element_start + quantum)][2][k] = element_volume

        return hole_sample_matrix

    def samples_generator_for_nn_training(self, data_directory):
        """
        This is generator function used in tensorflow.data.Dataset.from_generator formula.
        It is used during neural network training to provide samples for training.

        :param data_directory: path to directory with data samples
        :return: one data sample converted to proper format for nn
        """
        for file in glob.glob(data_directory + "*.mid"):
            notes = self.convert_one_midi_file_to_piano_roll_vector(file)

            yield np.array(notes)

    def get_pitches_intersection(self, first, second):
        """
        Used to get  pitches intersections from different neighbour time quantums

        :param first: list of pitches as integers
        :param second: list of pitches as integers
        :return:  intersection of two lists with pitches
        """
        return set(first).intersection(second)

    def create_pitches_list_from_ints(self, pitches_as_ints):
        """
        :param pitches_as_integers: midi numbers
        :return: list of sounds names created with midi numbers
        """
        pitches_list = []
        for int_pitch in pitches_as_ints:
            pitches_list.append(self.inverse_dictionary[int(int_pitch)])
        return pitches_list

    def create_music21_object(self, pitches, curr_offset, curr_duration, curr_volume):
        """
        :param pitches: pitches as integers which will create music21 object: note, chord or rest
        :param curr_offset: ofset of object to be created
        :param curr_duration: duration of object to be created
        :param curr_volume: volume of object to be created
        :return: music21 object: note, chord or rest
        """

        d = duration.Duration()
        d.quarterLength = curr_duration * self.time_quantum + self.time_quantum  # time_quantum is a duration of first time quantum of pitches' occurence

        curr_offset *= self.time_quantum

        # create rest object
        if 0 in set(pitches):
            r = note.Rest()
            r.duration = d
            r.offset = curr_offset
            return r
        # create note object
        elif len(pitches) == 1:
            n = note.Note(self.inverse_dictionary[int(pitches[0])])
            n.duration = d
            n.offset = curr_offset
            n.volume = curr_volume
            return n
        # create chord object
        else:
            pitches_list = self.create_pitches_list_from_ints(pitches)
            ch = chord.Chord(pitches_list)
            ch.duration = d
            ch.offset = curr_offset
            ch.volume = curr_volume
            return ch

    def create_music21_objects_started_at_given_time_quantum(self, output_matrix, curr_pitches, start_time_quantum_iter):
        """
        Gets all musical structures started at given time quantum,
        groups them besed on their duration time
        count average volume for every group
        and create objects

        :param output_matrix: music sample matrix returned by nn
        :param curr_pitches: pitches to build music21 objects
        :param start_time_quantum_iter: time quantum where objects will start
        :return: list of created music21 objects
        """
        # list for music21 objects
        objects_list = []

        # set for all different duration which occured in given set of pitches
        occuring_durations = set()
        occuring_durations = self.get_all_occuring_durations(output_matrix, curr_pitches, start_time_quantum_iter)

        # create as many music21 objects as different durations
        for dur in occuring_durations:
            pitches = self.get_pitches_of_given_duration(output_matrix, curr_pitches, start_time_quantum_iter, dur)
            curr_volume = self.count_average_volume(output_matrix, pitches, start_time_quantum_iter, dur)
            objects_list.append(self.create_music21_object(pitches, start_time_quantum_iter, dur, curr_volume))

        return objects_list

    def get_all_occuring_durations(self, output_matrix, curr_pitches, start_time_quantum_iter):
        """
        :param output_matrix: music sample matrix returned by nn
        :param curr_pitches: pitches currently processed to build music21 objects
        :param start_time_quantum_iter: time quantum where objects will start
        :return:
        """
        occuring_durations = []
        for p in curr_pitches:
            occuring_durations.append(output_matrix[start_time_quantum_iter][3][p])
        return set(occuring_durations)

    def get_pitches_of_given_duration(self, output_matrix, curr_pitches, start_time_quantum_iter, dur):
        """
        :param output_matrix:
        :param curr_pitches:
        :param start_time_quantum_iter:
        :param dur:
        :return:
        """
        pitches_list = []
        for p in curr_pitches:
            if output_matrix[start_time_quantum_iter][3][p] == dur:
                pitches_list.append(p)
        return np.array(pitches_list)

    def reset_already_processed_elements(self, output_array, pitches_to_clear, start_time_quantum):
        """
        Sets zeroes in nn output array at the place of already processed elements
        element_offset and element_duration are given in a time quantums, not in music21 unit

        :param output_array: array returned by nn
        :param pitches_to_clear: pitches which occured at given time quantum and have been already processed
        :param start_time_quantum: time quantum when given pitches started
        """
        """  """
        for element in pitches_to_clear:
            element_duration = output_array[start_time_quantum][3][element]
            for i in range(int(start_time_quantum), int(start_time_quantum + element_duration + 1)): #  + 1 is because element_duration should be initialized with 1 at the beggining and it is not
                output_array[i][0][element] = 0
                output_array[i][1][element] = 0
                output_array[i][2][element] = 0
                output_array[i][3][element] = 0

    def count_average_volume(self, output_array, pitches, element_offset, element_duration):
        """
        Volume values returned by nn are not always ideal,
        that is why different volume values for pitches from one chord can occur.
        To solve this problem average volume is counted. It is done by
        considering volumes from first time quantum when the sound occured
        It can be also changed and average volume can be counted
        with use all returned volumes from all time quantums where sound occurs

        :param output_array: music sample array returned by neural network
        :param pitches: pitches to count average volume for
        :param element_offset: offset of element for which average volume is counted
        :param element_duration: duration of element for which average volume is counted
        :return: average volume of music21 object
        """

        volume_sum = 0
        for p in pitches:
            volume_sum += output_array[element_offset][2][int(p)]
        return volume_sum/len(pitches)
        # return 0.5

    def is_sound_repeted(self, output_array, curr_pitches, curr_time_quantum):
        """
        Checks if sound is repeted or continued
        If in repeat vector at the indexes equivalent to pitches
        there is more 1s than 0s than sound is considered as repeated
        otherwise it is considered as continued

        :param output_array: array returned by nn
        :param curr_pitches: pitches to check if they are reeated in a given time quantum
        :param curr_time_quantum: current time quantum wher pitches occure
        :return: decision if pitches are repeated or no
        """

        sum = 0
        for p in curr_pitches:
            if output_array[curr_time_quantum][1][p] > 0.5:
                sum += 1
        if len(curr_pitches) - sum < sum:
            return True
        else:
            return False

    def update_pitches_duration_dimension(self, output_matrix, start_time_quantum, continued_list):
        """
        :param output_matrix:
        :param start_time_quantum: time quantume where duration value is increased for given pitches
        :param continued_list: pitches to increase their duration
        """
        for element in continued_list:
            output_matrix[start_time_quantum][3][element] = output_matrix[start_time_quantum][3][element] + 1

    def mark_and_get_pitches_that_are_continued(self, output_matrix, intersection, curr_time_quantum, start_time_quantum):
        """
        :param output_matrix:
        :param intersection: pitches to check if sound is repeated or continued
        :param curr_time_quantum: time quantum where piteches are checked
        :param start_time_quantum: time quantum where checked pitches started
        :return: list of pitches that where continued
        """
        repeated_list  = []
        continued_list = []

        for element in intersection:
            repeated = output_matrix[curr_time_quantum][1][element]
            if repeated > self.repeat_threshold:
                repeated_list.append(element)
            else:
                continued_list.append(element)

        self.update_pitches_duration_dimension(output_matrix, start_time_quantum, continued_list)

        return np.array(continued_list)

    def append_objects_to_list(self, objects, objects_list):
        """
        Appends objects to given list

        :param objects: objects to be appended to given list
        :param objects_list: list where objects are appended
        """
        for ob in objects:
            objects_list.append(ob)

    def convert_one_neural_network_output_sample_to_midi(self, output_sample, output_directory, file_names=""):
        """
        Converts nn output into music21 objects and saves them to midi files

        :param output_sample: matrix returned by neuran network
        :param output_directory: directory where nn output is supposed to be saved
        :param file_names: this name can specify list with file names which should be used to name and save output from nn
        :param time_quantum: it determine how nn samples are quantized in time
        :return: stream which can be save to midi file
        """

        sample_length = output_sample.shape[0]  # sample length given in time quantums
        # print("stample length: " + str(sample_length))
        # This matrix is auxiliary 4th dimension of nn output matrix
        # It is used to write for how many time quantums pitch lasts
        # Different pitches from the same time quantums can lasts different time quantums
        # as they can belong to separate objects eg. to note and chord
        auxiliary_matrix = np.zeros((sample_length, 1, 89))

        # auxiliary matrix is added to nn output_matrix
        output_matrix = np.concatenate((output_sample, auxiliary_matrix), axis=1)
        # print(output_matrix.shape)

        # list to hold created music21 objects and to save them to stream later
        music21_objects_list = []

        # iterators initialization
        start_time_quantum_iter = 0
        next_time_quantum_iter = start_time_quantum_iter

        # iteration with the use of 'next_time_quantum_iter' aim is to count continuous occureces
        # of the same pitches and save their number of time quantums to matrix' 4th dimension
        while start_time_quantum_iter < sample_length:

            # rewind time quantums with all values equal zeroes
            curr_pitches = (np.where(output_matrix[start_time_quantum_iter][0] > self.play_threshold))[0]  # todo: make it a function
            while len(curr_pitches) == 0:

                if start_time_quantum_iter >= sample_length - 1:
                    break
                start_time_quantum_iter += 1
                curr_pitches = (np.where(output_matrix[start_time_quantum_iter][0] > self.play_threshold))[0]

            # additional check to prevent out of bound error
            if start_time_quantum_iter >= sample_length-1:
                break

            # set next_time_quantum_iter to point at next pitches after start_time_quantum_iter
            next_time_quantum_iter = start_time_quantum_iter + 1

            # get pitches from the next time quantums and iterate until intersection will be empty
            next_pitches = (np.where(output_matrix[next_time_quantum_iter][0] > self.play_threshold))[0]
            intersection = self.get_pitches_intersection(curr_pitches, next_pitches)

            while len(intersection) > 0:
                # check which pitches from intersection are continued
                continued_pitches = self.mark_and_get_pitches_that_are_continued(output_matrix, intersection, next_time_quantum_iter, start_time_quantum_iter)

                # set next_time_quantum_iter to point at next pitches after start_time_quantum_iter
                next_time_quantum_iter += 1

                if next_time_quantum_iter >= sample_length:
                    break

                # get pitches from the next time quantums and iterate until intersection will be empty
                next_pitches = (np.where(output_matrix[next_time_quantum_iter][0] > self.play_threshold))[0]

                intersection = self.get_pitches_intersection(continued_pitches, next_pitches)

            # create objects which starts at current start_time_quantum_iter and add them to the music21_objects_list
            just_created_objects = self.create_music21_objects_started_at_given_time_quantum(output_matrix, curr_pitches, start_time_quantum_iter)
            self.append_objects_to_list(just_created_objects, music21_objects_list)

            # set zeroes in each dimension at places of pitches from which object was created
            self.reset_already_processed_elements(output_matrix, curr_pitches, start_time_quantum_iter)

            # increase start_time_quantum_iter
            start_time_quantum_iter += 1

            # check to prevent out of bound error
            if start_time_quantum_iter >= sample_length:
                break

        # for testing purposes
        # print("music21 objects length: " + str(len(music21_objects_list)))
        # for e in music21_objects_list:
        #     print(str(e) + " " + str(e.offset) + " " + str(e.duration))

        # create stream from music21 objects
        s = stream.Stream(music21_objects_list)

        # play generated sample
        # s.show('midi')

        # save stream to file
        s.write('midi', output_directory)

        return s

    def save_file_name_to_list(self, names_to_save, file):
        """
        Saves given file name to specified list
        """
        fname = str(file).split('\\')[-1:][0]
        if names_to_save == "test":
            self.test_file_names.append(fname)
        elif names_to_save == "train":
            self.train_file_names.append(fname)
        elif names_to_save == "validate":
            self.validate_file_names.append(fname)
        else:
            return

    def get_output_file_names(self, file_names):
        if file_names == "test":
            return self.test_file_names
        elif file_names == "train":
            return self.train_file_names
        elif file_names == "validate":
            return self.validate_file_names
        else:
            return []

    def print_test_file_names(self):
        """
        For testing purposes
        """
        print(self.test_file_names)

    def print_train_file_names(self):
        """
        For testing purposes
        """
        print(self.train_file_names)

    def print_validate_file_names(self):
        """
        For testing purposes
        """
        print(self.validate_file_names)


if __name__ == '__main__':
    t = FullPianoRollConverter()







