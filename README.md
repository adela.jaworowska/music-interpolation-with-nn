# Music-interpolation 

### Celem projektu jest interpolacja kawałków muzycznych z użyciem sieci neuronowych. Wykorzystana do tego celu architektura sieci to autoenkoder zawierający warstwy LSTM.

### Opis komponentów projektu

#### film
Folder zawiera film, w którym nagrany jest ekran komputera i przedstawione jest działanie stworzonej aplikacji. 

#### exampleSamples
Folder zawiera kilka przykładowych rezultatów otrzymanych w procesie interpolacji fragmentów muzycznych. W osobnych podfolderach znajdują się rezultaty otrzymane w wyniku intepolacji par utworów. Rezultaty te są w formacie MIDI, oraz plików PNG zawierających piano roll.

#### GUI
Interfejs graficzny został stworzony z wykorzytaniem biblioteki Tkinter.
Folder GUI zawiera następujące pliki:
* main.py - plik, który należy uruchomić
* config.py - zawiera dane konfiguracyjne do aplikacji graficznej
* window.py - zawiera cały kod tworzący grafikę oraz funckje ją obsługujące 
* exampleSamples - folder z fragmentami muzycznymi w formacie MIDI do udostępnionymi do interpolowania w aplikacji graficznej
* interpolationOutput - folder na pliki powstałe w wyniku interpolacji, konwencja nazewnictwa tych plików jest następująca:
[nazwa pierwszego utworu]\_[nazwa drugiego utworu]\_[długość interpolacji]\_[rok]\_[miesiąc]\_[dzień]\_[godziny]\_[minuty]\_[sekundy].mid

#### midiScripts
Folder zawiera skrypty służące do przetwarzania plików midi:
* full_piano_roll_converter.py - zawiera funckje służące do przetowrzenia plików midi do formatu odpowiedniego dla modelu sieci neuronowej oraz tego co zwróciła sieć z powrotem na pliki midi. Format do jakiego fragmenty muzyczne są przetwarzane opisany jest w tekście pracy.  
* midi_preprocesor.py - zawiera funckje pomocnicze, które używane były do przygotowania zbioru danych dla sieci neuronowej, np. funkcją, która tnie utwory na kawałki o zadanej liczbie kwantów czasu oraz do lepszego zbadania plików z bazy, np drukowanie długości plików itp. 
* interpolator.py - klasa służąca do wykonywania interpolacji na dwóch utworach

#### data
* musicDataHoleSamplesTraditionalSongs - folder zawiera wszystkie pliki MIDI (w całości) z bazy, która została użyta w projekcie.
* 96TimeQuantum_traditionalmusic -  folder zawiera dane użyte do tranowania modelu sieci, czyli pliki muzyczne z folderu data/musicDataHoleSamplesTraditionalSongs pocięte na fragmenty o długości 96 kwantów czasu każdy  

#### model
* LSTMAutoencoder_loss0_19181.py - stworzony model sieci neuronowej, wykorzytsywany w aplikacji graficznej
* LSTMAutoencoder_loss0_19181.h5 - wagi wytrenowanego modelu


#### LSTMAutoencoder_loss0_19181.ipynbt 
Plik notatnika jupyter notebook, w którym możliwe jest uruchomienie treningu modelu sieci. W pliku znajduje się odwołanie do pliku base.py, w celu pobrania danych treningowych.

#### base.py
Zawiera dane konfiguracyjne tzn. ścieżki do folderów z danymi, na których była uczona sieć, wykorzystywane podczas trenowania sieci

#### environmen.yml
Plik zawiera wszystkie pakiety i biblioteki wraz z wersjami, które nażeży zainstalować w środowisku języka python aby możliwe było uruchomienie aplikacji


Materiały zawarte w forderach /film oraz /exampleSamples w pełni pokazują jak działa stworzona aplikacja, oraz jakie rezultaty można uzyskać. Gdyby była potrzeba na uruchomienie aplikacji, poniżej przedstawiona jest instrukcja w jaki sposób należy to zrobić:
### Instrukcja uruchomienia projektu
Aby uruchomić aplikację graficzną należy:
1. Zainstalować narzędzie Anaconda - zarządce pakietów języka python 
2. Za pomocą Anacondy stworzyć środowisko: conda create --name myenv
3. Zainstalowac w stworzonym środowisku wszytskie wmagane biblioteki, podane w pliku envirnment.yml
4. Zainstalowac program PyCharm
5. W programie PyCharm otworzyć projekt i ustawić jego interpreter na plik python.exe z folderu zainstalowanego wcześniej środowiska
6. Uruchomić plik main.py 


